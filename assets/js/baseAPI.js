// 请求拦截器, 发送任何ajax都会触发的函数
$.ajaxPrefilter((option) => {
  option.url = 'http://big-event-api-t.itheima.net' + option.url
  // 判断url包含/my的, 配置请求头
  if (option.url.indexOf('/my') !== -1) {
    option.headers = {
      Authorization: localStorage.getItem('token')
    }
    // 这么写会不会有问题?
    // option.headers.Authorization = localStorage.getItem('token')
  }
  // complate函数无论成功还是失败都会触发
  option.complete = function ({ responseJSON }) {
    // res.status === 1 && 身份认证失败！   说明咱们要清空token,强制跳转到登录页  注意: 身份认证失败！必须复制
    if (responseJSON.status === 1 && responseJSON.message === '身份认证失败！') {
      // 清空token
      localStorage.removeItem('token')
      // 跳转到登录页
      location.href = '/login.html'
    }
  }
})
