$(function () {
  getUserInfo()

  // 点击退出按钮, 实现退出功能
  $('#btnLogout').on('click', function () {
    // 利用layui的弹出提示框
    layer.confirm('是否确定退出?', { icon: 3, title: '提示' }, function (index) {
      //do something
      // 清空token
      localStorage.removeItem('token')
      // 清空info
      localStorage.removeItem('info')
      // 跳转到登录页
      location.href = '/login.html'
      layer.close(index);
    });
  })
})

// 获取用户基本信息
// 可能发生的状况
// ①正常登录,token合法
// ②非正常登录, 强制访问index.html, 没有token    后端返回{"status":1,"message":"身份认证失败！"}
// ③非正常登录, 强制访问index.html, 伪造的token  后端返回{"status":1,"message":"身份认证失败！"}
function getUserInfo () {
  $.ajax({
    url: '/my/userinfo',
    // success到底什么时候触发? 只要后端正常返回数据(对,错),都会触发success
    success (res) {
      // res.status === 1 && 身份认证失败！   说明咱们要清空token,强制跳转到登录页  注意: 身份认证失败！必须复制
      // if (res.status === 1 && res.message === '身份认证失败！') {
      //   // 清空token
      //   localStorage.removeItem('token')
      //   // 跳转到登录页
      //   location.href = '/login.html'
      // }
      // 判断状态码如果不是0, 可能是1,2,3,4
      if (res.status !== 0) {
        // return作用是阻止后续代码继续运行
        return layer.msg(res.message)
      }
      // 存到本地
      localStorage.setItem('info', JSON.stringify(res.data))
      // 渲染用户信息
      renderAvatar(res.data)
    }
  })
}
// 渲染用户信息的方法
function renderAvatar ({ nickname, username, user_pic }) {
  // nickname优先 
  // 或运算找 真
  // 1 || 0 ---> 1
  // 0 || 1 ---> 1
  // 0 || '' --->  ''
  // 0 || 3 < 2 ---> 3 < 2 ---> false
  // 1 || 2 ---> 1
  // 1 || 3 > 2 ---> 1
  // 3 > 2 || 1 ---->  true

  // 与运算找 假
  // 1 && 0 ---> 0
  // 0 && 1 ---> 0
  // 0 && '' ---> 0
  // 0 && 3 < 2 ---> 0
  // 1 && 2 ---> 2
  // 1 && 3 > 2 ---> 3 > 2 ---> true
  // 1 && 3 < 2 --->  3 < 2 ---> false
  // 3 > 2 && 1 ---->  1
  const name = nickname || username
  // 渲染welcome盒子
  $('#welcome').html(`欢迎  ${name}`)
  // 根据user_pic判断到底显示img还是text-avatar
  if (user_pic) {
    // img显示, 修改src,隐藏text-avatar
    $('.layui-nav-img').attr('src', user_pic).show()
    $('.text-avatar').hide()
  } else {
    //text-avatar显示, 显示name的首字母(大写), img隐藏
    const first = name[0].toUpperCase()
    $('.text-avatar').html(first).show()
    $('.layui-nav-img').hide()
  }
}