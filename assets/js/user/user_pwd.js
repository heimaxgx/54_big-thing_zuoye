$(function () {
  // 定义密码的校验规则
  const form = layui.form
  form.verify({
    pass: [
      /^[\S]{6,12}$/
      , '密码必须6到12位，且不能出现空格'
    ],
    // 新密码不能和旧密码一样
    samePwd (value) {
      if (value === $('[name="oldPwd"]').val()) return '新旧密码不能一致'
    },
    // 确认密码必须和新密码一致
    rePwd (value) {
      if (value !== $('[name="newPwd"]').val()) return '两次密码不一致'
    }
  });
  // 发送重置密码的请求
  $('.layui-form').on('submit', function (e) {
    e.preventDefault();
    // 发ajax
    $.ajax({
      url: '/my/updatepwd',
      method: 'POST',
      data: $(this).serialize(),
      success (res) {
        // 判断状态码如果不是0, 可能是1,2,3,4
        if (res.status !== 0) {
          // return作用是阻止后续代码继续运行
          return layer.msg(res.message)
        }
        // 更新成功
        $('[type="reset"]').click()
        // 重新登录
        // 登陆成功提示
        layer.msg(res.message)
        window.parent.location.href = '/login.html'
      }
    })
  })
})