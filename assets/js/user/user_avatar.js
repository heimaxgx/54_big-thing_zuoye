$(function () {
  // 1.1 获取裁剪区域的 DOM 元素
  var $image = $('#image')
  // 1.2 配置选项
  const options = {
    // 纵横比
    aspectRatio: 1,
    // 指定预览区域
    preview: '.img-preview'
  }
  // 1.3 创建裁剪区域
  $image.cropper(options)

  // 点击上传按钮, 触发file文件框的点击事件
  $('#btnChooseImage').on('click', function () {
    $('#file').click()
  })

  // 注册文件上传的change事件
  $('#file').on('change', function (e) {
    // console.log(e.target);
    // console.dir($(this)[0]);
    // 获取上传的文件伪数组
    const files = $(this)[0].files  //只是文件伪数组 
    // 如果files长度为0. 说明用户取消了上传, 后面的代码就不需要执行了
    if (files.length === 0) {
      return layer.msg('请上传文件!')
    }
    // 获取到真实的文件
    const file = files[0]
    // 转换成url地址
    const url = URL.createObjectURL(file)
    // $('#img').attr('src', url)
    // 重置剪裁区域
    $image
      .cropper('destroy')      // 销毁旧的裁剪区域
      .attr('src', url)  // 重新设置图片路径
      .cropper(options)        // 重新初始化裁剪区域
  })

  // 点击确定按钮, 生成base64图片, 然后发ajax
  $('#btnUpload').on('click', function () {
    var dataURL = $image
      .cropper('getCroppedCanvas', { // 创建一个 Canvas 画布
        width: 100,
        height: 100
      })
      .toDataURL('image/png')
    // 发ajax
    $.ajax({
      url: '/my/update/avatar',
      data: {
        avatar: dataURL
      },
      method: 'POST',
      success (res) {
        // 判断状态码如果不是0, 可能是1,2,3,4
        if (res.status !== 0) {
          // return作用是阻止后续代码继续运行
          return layer.msg(res.message)
        }
        layer.msg(res.message)
        window.parent.getUserInfo()
      }
    })
  })
})