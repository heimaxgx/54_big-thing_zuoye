$(function () {
  // 用户基本信息校验
  const form = layui.form
  // 昵称不超过6位
  form.verify({
    nickname: function (value, item) {
      if (value.length > 6) {
        return '用户昵称不能超过6位'
      }
    }
  })
  // 从本存储取出用户信息
  const info = JSON.parse(localStorage.getItem('info'))
  // 通过info渲染基本资料
  //给表单赋值
  form.val("formUserInfo", info);

  // 表单重置
  $('#btnReset').on('click', function (e) {
    // 阻止默认行为
    e.preventDefault();
    // 重新给表单赋值
    form.val("formUserInfo", info);
  })

  // 点击提交修改按钮， 发送ajax
  $('.layui-form').on('submit', function (e) {
    // 阻止默认行为
    e.preventDefault();
    // console.log($('.layui-form').serialize());
    // console.log(form.val("formUserInfo"));
    $.ajax({
      url: '/my/userinfo',
      method: 'POST',
      data: {
        id: info.id,
        ...form.val("formUserInfo")
      },
      // data: `id=${info.id}&` + $(this).serialize(),
      success (res) {
        // 判断状态码如果不是0, 可能是1,2,3,4
        if (res.status !== 0) {
          // return作用是阻止后续代码继续运行
          return layer.msg(res.message)
        }
        // 成功， 调用父作用域的获取用户信息的方法
        window.parent.getUserInfo()
      }
    })
  })
  // 快速获取表单内容（新得数据，不能是info）
})
