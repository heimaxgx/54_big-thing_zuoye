$(function () {
  // 声明获取文章分类列表的函数
  function getArtList () {
    $.ajax({
      url: '/my/article/cates',
      success (res) {
        if (res.status !== 0) {
          // return作用是阻止后续代码继续运行
          return layer.msg(res.message)
        }
        const html = template('tpl-table', res)
        // console.log(html);
        $('tbody').html(html)
      }
    })
  }
  getArtList()
  let index = null;
  const form = layui.form
  // 增  添加文章类别
  $('#btnAddCate').on('click', function () {
    // 弹框
    index = layer.open({
      type: 1,
      area: ['500px', '250px'],
      title: '文章分类',
      content: $('#dialog-add').html()
    });

    // // 注册submit事件
    // $('#form-add').on('submit', function (e) {
    //   e.preventDefault();
    //   // 发请求
    //   $.ajax({
    //     type: "POST",
    //     url: "/my/article/addcates",
    //     data: $(this).serialize(),
    //     success: function (res) {
    //       if (res.status !== 0) {
    //         // return作用是阻止后续代码继续运行
    //         return layer.msg(res.message)
    //       }
    //       // 新增成功
    //       // ①关闭弹层
    //       layer.close(index)
    //       // ②更新文章分类
    //       getArtList()
    //     }
    //   });
    // });
    // console.log($('#form-add')[0]);
  })

  // 注册添加分类弹框里的submit事件
  $('body').on('submit', '#form-add', function (e) {
    e.preventDefault();
    // 发请求
    $.ajax({
      type: "POST",
      url: "/my/article/addcates",
      data: $(this).serialize(),
      success: function (res) {
        if (res.status !== 0) {
          // return作用是阻止后续代码继续运行
          return layer.msg(res.message)
        }
        // 新增成功
        // ①关闭弹层
        layer.close(index)
        // ②更新文章分类
        getArtList()
      }
    });
  });

  // 改  点击编辑按钮, 弹层
  $('body').on('click', '.btn-edit', function () {
    const id = $(this).attr('data-id')
    // 弹层
    index = layer.open({
      type: 1,
      area: ['500px', '250px'],
      title: '编辑分类',
      content: $('#dialog-edit').html()
    });
    // 发获取分类详情的ajax
    $.ajax({
      url: "/my/article/cates/" + id,
      success: function (res) {
        if (res.status !== 0) {
          // return作用是阻止后续代码继续运行
          return layer.msg(res.message)
        }
        // 利用layui-form的快速表单赋值功能
        form.val('form-edit', res.data)
      }
    });
  });

  // 点击确认修改按钮, 修改内容
  $('body').on('submit', '#form-edit', function (e) {
    e.preventDefault();
    // 发请求
    $.ajax({
      type: "POST",
      url: "/my/article/updatecate",
      data: $(this).serialize(),
      success: function (res) {
        if (res.status !== 0) {
          // return作用是阻止后续代码继续运行
          return layer.msg(res.message)
        }
        layer.msg(res.message)
        // 修改成功
        // ①关闭弹层
        layer.close(index)
        // ②更新文章分类
        getArtList()
      }
    });
  })

  // 删
  $('body').on('click', '.btn-delete', function () {
    const id = $(this).attr('data-id')
    // 弹询问框
    // 利用layui的弹出提示框
    layer.confirm('是否确定删除?', { icon: 3, title: '提示' }, function (index) {
      // 发获取分类详情的ajax
      $.ajax({
        url: "/my/article/deletecate/" + id,
        success: function (res) {
          if (res.status !== 0) {
            // return作用是阻止后续代码继续运行
            return layer.msg(res.message)
          }
          layer.msg(res.message)
          // 刷新页面
          getArtList()
        }
      });
      layer.close(index);
    });
  });
})