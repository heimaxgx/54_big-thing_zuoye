$(function () {
  // 初始化富文本编辑器
  initEditor()
  // 裁剪效果
  // 1. 初始化图片裁剪器
  var $image = $('#image')
  // 2. 裁剪选项
  var options = {
    aspectRatio: 400 / 280,
    preview: '.img-preview'
  }
  // 3. 初始化裁剪区域
  $image.cropper(options)
  // ①发送获取文章分类的ajax
  // 声明获取文章分类列表的函数
  const form = layui.form
  function getArtList () {
    $.ajax({
      url: '/my/article/cates',
      success (res) {
        if (res.status !== 0) {
          // return作用是阻止后续代码继续运行
          return layer.msg(res.message)
        }
        // 数据回显
        const html = template('tpl-cate', res)
        // 不会渲染成layui的下拉结构
        $('[name=cate_id]').html(html)
        // 表单更新渲染
        form.render()
      }
    })
  }
  getArtList()

  // 给选择封面按钮注册点击事件, 触发文件上传
  $('#btnChooseImage').on('click', function () {
    $('#coverFile').click()
  });

  // 注册文件上传的change事件
  $('#coverFile').on('change', function (e) {
    // console.log(e.target);
    // console.dir($(this)[0]);
    // 获取上传的文件伪数组
    const files = $(this)[0].files  //只是文件伪数组 
    // 如果files长度为0. 说明用户取消了上传, 后面的代码就不需要执行了
    if (files.length === 0) {
      return layer.msg('请上传文件!')
    }
    // 获取到真实的文件
    const file = files[0]
    // 转换成url地址
    const url = URL.createObjectURL(file)
    // 重置剪裁区域
    $image
      .cropper('destroy')      // 销毁旧的裁剪区域
      .attr('src', url)  // 重新设置图片路径
      .cropper(options)        // 重新初始化裁剪区域
  })

  // 定义一个全局变量
  let state = '已发布'
  // 点击存为草稿, 修改state为未发布
  $('#btnSave2').on('click', function () {
    state = '草稿'
  });

  // jq可以快速获取表单数据 $(this).serialize()  'key=value&key=value'   用于表单数据格式
  // 原生语法怎么快速获取表单数据  const fd = new FormData(表单DOM元素)  用于文件数据格式
  // ①注册表单提交事件
  $('#form-pub').on('submit', function (e) {
    e.preventDefault();
    // ②快速获取表单数据
    const fd = new FormData($(this)[0])
    // 误区: 不能使用对象.属性的方式访问和添加的
    // 必须循环才能看到
    // fd.forEach(v => console.log(v))
    // ③给formData追加数据
    fd.append('state', state)
    // 坑: 图片裁剪的回调函数是异步的, 所以ajax请求必须写在回调函数内
    $image
      .cropper('getCroppedCanvas', { // 创建一个 Canvas 画布
        width: 400,
        height: 280
      })
      .toBlob(function (file) {       // 将 Canvas 画布上的内容，转化为文件对象
        // 得到文件对象后，进行后续的操作
        fd.append('cover_img', file)
        // 在这发ajax
        pubArticle(fd)
      })
  });

  function pubArticle (fd) {
    $.ajax({
      type: "POST",
      url: "/my/article/add",
      data: fd,
      contentType: false,  // 这个是jq传文件格式数据的配置项, 不要背
      processData: false,  // 这个是jq传文件格式数据的配置项, 不要背
      success: function (res) {
        if (res.status !== 0) {
          // return作用是阻止后续代码继续运行
          return layer.msg(res.message)
        }
        // 触发文章列表点击事件
        window.parent.document.querySelector('#list').click()
      }
    });
  }
})