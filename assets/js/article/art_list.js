$(function () {
  template.defaults.imports.timestamp = function (value) {
    // 格式化成  2022-12-15 17:06:00
    // moment(value).format('YYYY-MM-DD HH:mm:ss');
    return moment(value).format('YYYY/MM/DD HH:mm:ss');
  };
  const form = layui.form
  const laypage = layui.laypage;
  // 定义查询参数(注释掉)
  const q = {
    pagenum: 1, //默认第一页
    pagesize: 2, //默认每一页看2条
    cate_id: '', //文章分类的id
    state: '' //文章的发布状态  
  }
  function getArtList () {
    $.ajax({
      url: '/my/article/cates',
      success (res) {
        if (res.status !== 0) {
          // return作用是阻止后续代码继续运行
          return layer.msg(res.message)
        }
        // 数据回显
        const html = template('tpl-cate', res)
        // 不会渲染成layui的下拉结构
        $('[name=cate_id]').html(html)
        // 表单更新渲染
        form.render()
      }
    })
  }
  getArtList()


  initTable()
  // 定义一个获取文章列表的ajax
  function initTable () {
    // 发获取分类详情的ajax
    $.ajax({
      url: "/my/article/list",
      data: q,
      success: function (res) {
        if (res.status !== 0) {
          // return作用是阻止后续代码继续运行
          return layer.msg(res.message)
        }
        // 如果res.data长度为0, 说明当前页没有数据, 让pagenum - 1, 重新发请求
        if(res.data.length === 0 && q.pagenum !== 1) {
          q.pagenum -= 1
          // 重新拉数据
          return initTable()
        }
        // 通过模板引擎渲染
        const html = template('tpl-table', res)
        $('tbody').html(html)

        // 渲染分页器, 需要total参数
        initPage(res.total)

      }
    });
  }
  // 筛选按钮点击事件
  $('#form-search').on('submit', function (e) {
    e.preventDefault();
    // 获取文章分类, 获取文章的状态
    const cate = $('[name="cate_id"]').val()
    const state = $('[name="state"]').val()
    // 修改q的值
    q.cate_id = cate
    q.state = state
    // 发ajajx
    initTable()
  });
  // 定义一个渲染分页器的方法
  function initPage(total) {
      // 调用layui提供的分页器方法
      laypage.render({
        elem: 'pageBox', //注意，这里的 test1 是 ID，不用加 # 号
        count: total, //数据总数，从服务端得到
        limit: q.pagesize,  //每页显示多少条
        curr: q.pagenum, //起始页
        limits: [2, 3, 5, 10],
        layout: ['count', 'limit','prev', 'page', 'next', 'skip'],
        jump: function(obj, first){ //laypage.render()一触发, 就执行jump函数
          // 如果在这里发送initTable(), 会发生什么事情?
          // 造成死循环
          //这里写业务逻辑
          if(!first){
            // initTable()应该写在这里
            // 修改页码值, 给发ajax做准备
            q.pagenum = obj.curr
            q.pagesize = obj.limit
            initTable()
          }
        }
      });
  }

  // 给删除按钮注册点击事件
  $('body').on('click', '.btn-delete', function () {
    // 拿id
    const id = $(this).attr('data-id')
    // 发送删除的ajax
    $.ajax({
      url: "/my/article/delete/" + id,
      success: function (res) {
        if (res.status !== 0) {
          // return作用是阻止后续代码继续运行
          return layer.msg(res.message)
        }
        layer.msg(res.message)
        // 重新拉数据渲染
        initTable()
      }
    });
  })
})