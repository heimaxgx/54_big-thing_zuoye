// 入口函数  
// 作用: ①等到页面中的DOM元素全部加载完毕执行   加载速度快, 不用等外部音频视频加载完毕
//       ②有自己独立的变量作用域    
$(function () {
  // ①点 '去注册账号'  让login-box隐藏,让reg-box显示
  $('#link_reg').on('click', function () {
    $('.login-box').hide()
    $('.reg-box').show()
  })
  // ①点 '去登录'  让login-box显示,让reg-box隐藏
  $('#link_login').on('click', function () {
    $('.login-box').show()
    $('.reg-box').hide()
  })

  // 拿到一个form对象
  var form = layui.form
  // 注册自定义校验方法
  form.verify({
    username: function (value, item) { //value：表单的值、item：表单的DOM对象
      if (!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)) {
        return '用户名不能有特殊字符';
      }
      if (/(^\_)|(\__)|(\_+$)/.test(value)) {
        return '用户名首尾不能出现下划线\'_\'';
      }
      if (/^\d+\d+\d$/.test(value)) {
        return '用户名不能全为数字';
      }
    }

    //我们既支持上述函数式的方式，也支持下述数组的形式
    //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
    , pass: [
      /^[\S]{6,12}$/
      , '密码必须6到12位，且不能出现空格'
    ],
    // 就是方法简写
    repass (value) {
      // ①获取密码框的内容
      const pass = $('.reg-box [name="password"]').val()
      // ②判断重复密码value和上一个密码框的内容是否一致
      // ③如果不一致, return一个提示信息
      if (value !== pass) return '两个密码不一致'
    }
  });

  // 注册
  // http://big-event-api-t.itheima.net/api/reguser

  // 思考: 快速获取form表单内容  
  // ①给form标签注册submit事件
  // ②调用$(this).serialize()
  $('#form_reg').on('submit', function (e) {
    // 阻止默认提交
    e.preventDefault();
    // 快速获取表单内容
    const formData = $(this).serialize()
    // 发送ajax请求
    $.ajax({
      url: '/api/reguser',
      method: 'post',
      data: formData,
      success (res) {
        // 判断状态码如果不是0, 有问题
        if (res.status !== 0) {
          // return作用是阻止后续代码继续运行
          return layer.msg(res.message)
        }
        // 注册成功
        layer.msg(res.message)
        // 通过js来点击 去登录按钮  一定要提前注册
        $('#link_login').click()
      }
    })
  })

  // 登录
  $('#form_login').on('submit', function (e) {
    // 阻止默认提交
    e.preventDefault();
    // 快速获取表单内容
    const formData = $(this).serialize()
    // 发送ajax请求
    $.ajax({
      url: '/api/login',
      method: 'post',
      data: formData,
      success (res) {
        // 判断状态码如果不是0, 有问题
        if (res.status !== 0) {
          // return作用是阻止后续代码继续运行
          return layer.msg(res.message)
        }
        // 登录成功
        layer.msg(res.message)
        // 重要:
        // ①页面跳转
        location.href = '/index.html'
        // ②存储token
        localStorage.setItem('token', res.token)
      }
    })
  })
})


